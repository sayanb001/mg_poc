##############################################################################
# For copyright and license notices, see __manifest__.py file in module root
# directory
##############################################################################
from odoo import models,fields, _
from ast import literal_eval


class PurchaseOrder(models.Model):
    _inherit = "purchase.order"
