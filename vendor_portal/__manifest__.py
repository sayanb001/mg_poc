##############################################################################
#
#    Copyright (C) 2015  ADHOC SA  (http://www.adhoc.com.ar)
#    All Rights Reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'Vendor Portal',
    'version': '13.0',
    'category': 'Vendor Portal',
    'sequence': 14,
    'author': 'Prolitus',
    'website': 'https://www.prolitus.com',
    'license': 'AGPL-3',
    'images': [
    ],
    'depends': [
        'purchase','mg_purchase'
    ],
    'data': [
        'security/purchase_security.xml',
        'security/ir.model.access.csv',
        'views/purchase_order_views.xml',
        'views/vendor_invoice_views.xml',
        'views/grn_views.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
