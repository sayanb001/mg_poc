from odoo import models, fields, api

class ResUser(models.Model):
    _inherit = ['res.users']

    @api.model
    def create(self,vals):
        res = super(ResUser,self).create(vals)
        job_position_arr = [
            (0,0,{'position':'CEO'}),
            (0,0,{'position':'President'}),
            (0,0,{'position':'Legal Representative'}),
            (0,0,{'position':'Finance Contact'}),
            (0,0,{'position':'Sales 2'}),
        ]
        reg_vals = {
            'res_user_id': res.id,
            'job_position_ids': job_position_arr,
        }
        self.env['vendor.registration'].sudo().create(reg_vals)
        return res