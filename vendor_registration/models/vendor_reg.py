# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError
import logging
_logger = logging.getLogger(__name__)

state_selection = [
    ('draft','Draft'),
    ('submitted','Submitted for Approval'),
    ('approved','Approved'),
    ('rejected','Rejected'),    
]
class VendorRegistration(models.Model):
    _name = 'vendor.registration'
    _inherit = 'mail.thread'
    _description = 'Vendor Registration Form'

    reason = fields.Text(string="Reason for Approval/Rejection",track_visibility="onchange")
    remarks = fields.Text(string="Remarks",track_visibility="onchange")
    state = fields.Selection(selection=state_selection, string="State", default="draft",track_visibility="onchange")
    supplier_name = fields.Char(string="Supplier Name")
    res_user_id = fields.Many2one('res.users', string="User")
    # registered_address = fields.Char(string="")
    reg_street1 = fields.Char(required=False)
    reg_street2 = fields.Char(required=False)
    reg_city = fields.Char(required=False)
    reg_state_id = fields.Many2one("res.country.state", string='State', ondelete='restrict', domain="[('country_id', '=?', reg_country_id)]")
    reg_zip = fields.Char(required=False)
    reg_country_id = fields.Many2one('res.country', string='Country', ondelete='restrict')
    biz_street1 = fields.Char(required=False)
    biz_street2 = fields.Char(required=False)
    biz_city = fields.Char(required=False)
    biz_state_id = fields.Many2one("res.country.state", string='State', ondelete='restrict', domain="[('country_id', '=?', biz_country_id)]")
    biz_zip = fields.Char(required=False)
    biz_country_id = fields.Many2one('res.country', string='Country', ondelete='restrict')

    bank_name_address = fields.Text(string="Bank name & Address")

    msme_reg = fields.Char(string="MSME Registered")
    business_scope = fields.Char(string="Business Scope")
    bank_acc_no = fields.Char(string="Bank Account Number")
    bank_ifsc_code = fields.Char(string="IFSC Code")

    reg_date = fields.Date(string="Registration Date", default=fields.Date.today)
    cin = fields.Char(string="CIN")
    pan = fields.Char(string="PAN")
    gstin = fields.Char(string="GSTIN")
    swift_code = fields.Char(string="Swift Code")

    job_position_ids = fields.One2many('vendor.job.info', 'vendor_reg_id', string=" ")

    product_turnover = fields.One2many('vendor.turnover', 'vendor_reg_id', string=" ")
    total_turnover = fields.Float(string="Total Turnover")
    total_assets = fields.Integer(string="Total Assets")
    total_staff = fields.Integer(string="Total Staff")

    email = fields.Char(string="Email")
    backup_email = fields.Char(string="Backup Email")
    fax = fields.Char(string="Fax")
    backup_fax = fields.Char(string="Backup Fax")

    iso90001_qualification = fields.Binary(attachment=True, string="ISO-90001 Qualification")
    iso14001_qualification = fields.Binary(attachment=True, string="ISO-14001 Qualification")
    ts_certificate = fields.Binary(attachment=True, string="TS Certificate")
    other = fields.Char(string="Other")

    # Main process and detection equipments
    vendor_eqip_ids = fields.One2many('vendor.equipments', 'vendor_reg_id', string=" ")

    vendor_branch_ids = fields.One2many('vendor.branches', 'vendor_reg_id', string=" ")
    top_customer_ids = fields.One2many('vendor.top_customers', 'vendor_reg_id', string=" ") #top 3 customers
    techniq_coop_ids = fields.One2many('vendor.techniq.coop', 'vendor_reg_id', string=" ")

    pay_ZN01 =  fields.Boolean(string="ZN01")
    pay_MN1 =  fields.Boolean(string="MN1")
    pay_MN2 =  fields.Boolean(string="MN2")
    pay_ZN14 =  fields.Boolean(string="ZN14")
    buyer = fields.Many2one('res.users', string="Buyers")
    check = fields.Char(string="Check")

    def name_get(self):
        result = []
        for record in self:
            name = self.res_user_id.name
            result.append((record.id, name))
        return result

    def btn_submit(self):
        self.state="submitted"

    def btn_reg_accept(self):
        if not self.reason:
            raise UserError('Reason for Approval/Rejection is mandatory for this action')
        self.state="approved"
        self.res_user_id.partner_id.registered_vendor = True

    def btn_reg_reject(self):
        if not self.reason:
            raise UserError('Reason for Approval/Rejection is mandatory for this action')
        self.state="rejected"

    def btn_reg_resumbit(self):
        self.state="draft"

class VendorJobInfo(models.Model):
    _name = 'vendor.job.info'

    vendor_reg_id = fields.Many2one('vendor.registration', string="Vendor Form ID")
    position = fields.Char(string="Position")
    name = fields.Char(string="Name")
    telephone = fields.Char(string="Telephone")
    fax = fields.Char(string="Fax")
    mobile = fields.Char(string="Mobile")
    email = fields.Char(string="Email")

class VendorTurnover(models.Model):
    _name = 'vendor.turnover'

    vendor_reg_id = fields.Many2one('vendor.registration', string="Vendor Form ID")
    prod_service_name = fields.Char(string="Product Service Name")
    turnover = fields.Float(string="Turnover")
    ratio_turnover = fields.Float(string="Ratio Turnover")
    turnover_attch = fields.Binary(attachment=True, string="Attachment")

class VendorEquipments(models.Model):
    _name = 'vendor.equipments'

    vendor_reg_id = fields.Many2one('vendor.registration', string="Vendor Form ID")
    equip_name = fields.Char(sting="Equiment Name")
    equip_model = fields.Char(sting="Model")
    equip_qty = fields.Integer(string="Quantity")
    equip_attch = fields.Binary(attachment=True, string="Attachment")

    
class VendorBranches(models.Model):
    _name = 'vendor.branches'

    vendor_reg_id = fields.Many2one('vendor.registration', string="Vendor Form ID")
    address = fields.Char(string="Address")
    telephone = fields.Char(string="Telephone")
    area = fields.Char(string="Plant Office Area")

class VendorTopCustomers(models.Model):
    _name = 'vendor.top_customers'

    vendor_reg_id = fields.Many2one('vendor.registration', string="Vendor Form ID")
    company_name = fields.Char(string="Company Name")
    major_prod_serve = fields.Char(string="Major Product Served")
    biz_total_amount = fields.Float(string="Business Total Amount")

class VendorTechniqueCooperation(models.Model):
    _name = 'vendor.techniq.coop'

    vendor_reg_id = fields.Many2one('vendor.registration', string="Vendor Form ID")
    coop_company_name = fields.Char(string="Cooperate Company Name")
    coop_proj_synopsis = fields.Binary(attachment=True, string="Cooperate Project Synopsis")
