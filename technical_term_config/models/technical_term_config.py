# -*- coding: utf-8 -*-
from odoo.exceptions import Warning, UserError
from odoo import models, fields, api
from datetime import datetime
import logging
_logger = logging.getLogger(__name__)


SEL_TYPE = [
    ('multi_choice', 'Multiple Choice'),
    ('int', 'Integer'),
    ('text', 'Text'),
    ('date', 'Date'),
]


class TechnicalTermConfig(models.Model):
    _name = 'technical.term.config'

    name = fields.Char(string="Configuration ID", readonly=True, required=True, copy=False, default='New')
    requisition_id = fields.Many2one('purchase.requisition', string="Purchase Requisition ID")
    description = fields.Text(string="Description", required=True)
    sel_type = fields.Selection(string="Type", selection=SEL_TYPE, required=True)
    document_check = fields.Boolean(string="Document ?",)
    score_check = fields.Boolean(string="Score ?", default=True)
    max_score = fields.Float(string="Maximum Score")
    answers = fields.One2many('technical.answer.terms', 'tt_config_id', string="Answers", copy=True)
    answers_visibility_check = fields.Boolean(string='Answers visible?')

    @api.onchange('sel_type', 'score_check')
    def _check_fields(self):
        if self.sel_type == 'multi_choice' or ( self.sel_type == 'int' and self.score_check) or (self.sel_type == 'date' and self.score_check):
            self.answers_visibility_check = True
        else:
            self.answers_visibility_check = False

    @api.model
    def create(self, vals):
        _logger.warning(' ::::: TTC VALS  %s ' % vals)
        if vals['sel_type'] == 'multi_choice' or (vals['sel_type'] == 'int' and vals['score_check']) or (vals['sel_type'] == 'date' and vals['score_check']):
            if 'answers' not in vals or len(vals['answers']) < 2:
                raise UserError('Please provide at least 2 Answer Terms.')
        # _logger.warning(' ::::: ANSWERS  %s ' % len(vals['answers']))
        res = super(TechnicalTermConfig, self).create(vals)
        if not res:
            return
        else:
            if res.name == 'New':
                res['name'] = self.env['ir.sequence'].next_by_code(self._name) or 'New'
            return res


class TechnicalAnswerTerms(models.Model):
    _name = 'technical.answer.terms'
    _description = "m2o model for answer field"

    tt_config_id = fields.Many2one('technical.term.config', string='Config ID')
    from_int = fields.Integer(string="From",)
    to_int = fields.Integer(string="To",)
    from_date = fields.Date(string="From",)
    to_date = fields.Date(string="To",)
    value = fields.Char(string="Value",)
    score = fields.Float(string="Score",)

    def str_to_date_obj(self, date_str):
        DATE_FORMAT = "%Y-%m-%d"
        return datetime.strptime(date_str, DATE_FORMAT).date()

    @api.model
    def create(self, vals):
        _logger.warning("\n\n:::: PARENT %s" % self._context)
        parent = self.env['technical.term.config'].search([('id', '=', vals['tt_config_id'])])
        error_msg = ''
        if not parent.answers:
            if int(vals['from_int']) > int(vals['to_int']):
                error_msg += f"From greater than To not possible. {frm} - {to}\n"
            if int(vals['score']) > parent.max_score:
                error_msg += f"Score value must be less than Maximum score: {vals['score']} \n"
            if error_msg:
                raise UserError(error_msg)
            return super(TechnicalAnswerTerms, self).create(vals)
        if parent.sel_type == 'int':
            frm = int(vals['from_int'])
            to = int(vals['to_int'])
            if frm > to:
                error_msg += f"From greater than To not possible. {frm} - {to}\n"
            _logger.warning("::::: FRM %s %s" %(frm, type(frm)))
            for answer in parent.answers:
                _logger.warning("::::: ANS FRM %s %s" %(answer.from_int, type(answer.from_int)))
                if(frm >= answer.from_int and frm <= answer.to_int) or (to >= answer.from_int and to <= answer.to_int) or (answer.from_int >= frm and answer.to_int <= to):
                    error_msg +='Integer values should not overlap with other answer values.\n'
        if parent.sel_type == 'date':
            frm = self.str_to_date_obj(vals['from_date'])
            to = self.str_to_date_obj(vals['to_date'])
            if frm > to:
                error_msg += f"From  greater than To not possible. {frm} - {to}\n"
            for answer in parent.answers:
                ans_frm_date = answer.from_date
                ans_to_date = answer.to_date
                if (frm >= ans_frm_date and frm <= ans_to_date) or (to >= ans_frm_date and to <= ans_to_date) or (ans_frm_date >= frm and ans_to_date <= to):
                    error_msg += "Date values should not overlap with other answer values\n"
        if int(vals['score']) > parent.max_score:
            error_msg += f"Score value must be less than Maximum score: {vals['score']} \n"
        if error_msg:
            raise UserError(error_msg)
        return super(TechnicalAnswerTerms, self).create(vals)
