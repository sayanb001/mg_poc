# -*- coding: utf-8 -*-
{
    'name': "Technical Term Configuration",

    'summary': """Technical Term Config""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'mg_tender'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'security/ttc_security.xml',
        'views/views.xml',
        'data/ir_sequence_data.xml'
    ],

    'installable': True,
    'application': True,
    'sequence': 1,
}