# -*- coding: utf-8 -*-

from odoo.exceptions import Warning, UserError
from odoo import models, fields, api
import logging
_logger = logging.getLogger(__name__)

# SEL_TYPE = [
#     ('multi_choice', 'Multiple Choice'),
#     ('int', 'Integer'),
#     ('text', 'Text'),
#     ('date', 'Date'),
# ]

class TechnicalTermConfigWizard(models.TransientModel):
    _name = 'technical.term.config.wizard'

    # description = fields.Text(string="Description", required=False)
    # sel_type = fields.Selection(string="Type", selection=SEL_TYPE, required=False)
    # document_check = fields.Boolean(string="Document ?",)
    # score_check = fields.Boolean(string="Score ?", default=True)
    # max_score = fields.Float(string="Maximum Score")
    # technical_term_id = fields.Many2one('technical.term.config', string="Technical Term ID")

    # ttc_m2m = fields.Many2many('technical.term.config', string="TTC_ID")


    # def process_selected_terms(self):
    #     ''' '''
    #     _logger.warning(":::: PROCESS SELF  %s" % self._context)
    #     active_id = self._context['active_id'] or self._context['params']['active_id']
    #     parent = self.env['purchase.requisition'].search([('id', '=', active_id)])
    #     for rec in  self.ttc_m2m:
    #         ans_arr = []
    #         for ans in rec.answers:
    #             a_data = {
    #                 # "tt_term_id": !?
    #                 "from_int": ans.from_int,
    #                 "to_int": ans.to_int,
    #                 "from_date": ans.from_date,
    #                 "to_date": ans.to_date,
    #                 "value": ans.value,
    #                 "score": ans.score,
    #             }
    #             ans_arr.append((0,0,a_data))
    #         #     created = list(self.env['tender.answer.terms'].create(a_data))
    #         # _logger.warning("::::: created :: %s" % created)
    #         data = {
    #             "requisition_id": parent.id,
    #             "description": rec.description,
    #             "sel_type": rec.sel_type,
    #             "document_check": rec.document_check,
    #             "score_check": self.score_check,
    #             "max_score": rec.max_score,
    #             "answers":ans_arr,
    #             "answers_visibility_check": rec.answers_visibility_check,
    #         }
    #         self.env['tender.technical.terms'].create(data)
    #     _logger.warning("\n\n:::: SELF TTC :: %s" % self.ttc_m2m)
