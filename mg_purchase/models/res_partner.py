# -*- coding: utf-8 -*-
from odoo.exceptions import Warning, UserError
from odoo import models, fields, api
import logging
_logger = logging.getLogger(__name__)

class ResPartner(models.Model):
    _inherit = ["res.partner"]
    registered_vendor = fields.Boolean(string='Registered Vendor?', default=False)
