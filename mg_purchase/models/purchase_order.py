# -*- coding: utf-8 -*-
from odoo.exceptions import Warning, UserError
from odoo import models, fields, api
import logging
_logger = logging.getLogger(__name__)

state_options = [
    ('draft', 'RFQ'),
    ('sent', 'RFQ Sent'),
    ('to approve', 'To Approve'),
    ('awaiting_approval', 'Awaiting Approval'),
    ('approved', 'Approved'),
    ('purchase', 'Purchase Order'),
    ('done', 'Locked'),
    ('cancel', 'Cancelled')
    ]

class PurchaseOrder(models.Model):
    _inherit = ['purchase.order']
    techno_com_bid = fields.Boolean(string="Techno-commercial Bid")
    technical_terms = fields.One2many('technical.terms','po_id',string="Technical Terms")
    state = fields.Selection(selection=state_options, string="Status", readonly=True, index=True, copy=False, default='draft', tracking=True)
    total_score = fields.Float(string='Total Score')
    is_qualified =  fields.Boolean(string="Qualified?")
    presentation_points =  fields.Float(string="Presentation Points")
    quote_2 = fields.Monetary(string="2nd Quote", currency_field='currency_id')
    quote_follow = fields.Monetary(string="Follow on Quote", currency_field='currency_id')
    rel_registered_vendor = fields.Boolean(string="Registered Vendor?", related='partner_id.registered_vendor')
    complete_score = fields.Float(string="Total Score Gained", compute="_compute_complete_score")
    maxi_score = fields.Float(string="Total Maximum Score",compute="_compute_max_score")

    def update_quotes(self):
        obj = self.requisition_id.related_rfq_ids.search([('po_id', '=', self.id)])
        obj.quote_2 = self.quote_2
        obj.quote_follow = self.quote_follow
        obj.total_score = self.complete_score

    def action_rfq_send(self):
        if not self.rel_registered_vendor:
            raise UserError('Vendor is not Registered')
        for line in self.order_line:
            if not line.product_qty:
                raise UserError('Please check product quantity. It must not be 0.')
        return super(PurchaseOrder, self).action_rfq_send()

    @api.onchange('technical_terms')
    def _compute_complete_score(self):
        self.complete_score = sum(term.score_gained for term in self.technical_terms)

    @api.onchange('technical_terms')
    def _compute_max_score(self):
        sc_lst = 0.0
        for term in self.technical_terms:
            if term.max_score:
                sc_lst += term.max_score
        self.maxi_score = sc_lst


    # @api.onchange('requisition_id')
    # def _onchange_requisition_id(self):
    #     super(PurchaseOrder,self)._onchange_requisition_id()
    #     if not self.requisition_id:
    #         return
    #     self.techno_com_bid = self.requisition_id.type_id.techno_com_bid
    #     requisition = self.requisition_id
    #     term_arr = []
    #     for term in requisition.technical_terms:
    #         frm_old_term = {
    #             "po_id": self.id,
    #             "sourcing_technical_terms": term.id,
    #             "selected_term_doc": term.document_check,
    #             "selected_term_type": term.sel_type,
    #         }
    #         term_arr.append((0,0,frm_old_term))
    #     self.technical_terms = term_arr
    #     _logger.warning("\n\n:::::TERM_ARR::: %s" % term_arr)

    def btn_submit_terms(self):
        err_msgs = []
        # for term in self.technical_terms:
        #     if term.status == 'approved':
        #         continue
        #     term_err = ''
        #     value_fields = [term.data_date, term.data_int, term.data_text, term.data_multi_choice]
        #     if not list(filter(lambda val: val, value_fields)):
        #         term_err += f"Value field is not filled. Please check {term.sourcing_technical_terms.name}\n"
        #     # if term.selected_term_doc and not term.document:
        #     #     term_err += f"Document field is not filled. Please check {term.sourcing_technical_terms.name}\n"
        #     if term_err:
        #         err_msgs.append(term_err)
        #     else:
        #         term.status = 'await_approval'
        # if err_msgs:
        #         raise UserError('\n'.join(err_msgs))
        if not self.partner_id.registered_vendor:
            raise UserError('Vendor is not registered')
        self.state = "awaiting_approval"

    # def reset_terms(self):
    #     for term in self.technical_terms:
    #         if term.status != 'approved':
    #             term.status = 'resubmit'

    def approve(self):
        self.write({'state':'approved'})

    def button_confirm(self):
        for order in self:
            if order.state == 'approved':
                order._add_supplier_to_product()
                # Deal with double validation process
                if order.company_id.po_double_validation == 'one_step'\
                        or (order.company_id.po_double_validation == 'two_step'\
                            and order.amount_total < self.env.company.currency_id._convert(
                                order.company_id.po_double_validation_amount, order.currency_id, order.company_id, order.date_order or fields.Date.today()))\
                        or order.user_has_groups('purchase.group_purchase_manager'):
                    order.button_approve()
                else:
                    order.write({'state': 'to approve'})
            return super(PurchaseOrder, self).button_confirm()

    @api.model
    def create(self,vals):
        res = super(PurchaseOrder, self).create(vals)
        rank_data = {
                'po_id': res.id,
                'requisition_id': res.requisition_id.id,
                'state': res.state,
                'partner_id': res.partner_id.id,
            }
        self.env['vendor.tender.ranks'].create(rank_data)
        return res


class TechnicalTerms(models.Model):
    _name = 'technical.terms'

    # STATUS_TYPE = [
    #    ('approved', "Approved"),
    #    ('rejected', "Rejected"),
    #    ('resubmit', "Re-submission"),
    #    ('await_approval', "Awaiting Approval"),
    #    ('await_submission', "Awaiting Submission"),
    # ]

    # SEL_TYPE = [
    #     ('multi_choice', 'Multiple Choice'),
    #     ('int', 'Integer'),
    #     ('text', 'Text'),
    #     ('date', 'Date'),
    # ]

    po_id = fields.Many2one('purchase.order', string="Purchase Order ID")
    sourcing_technical_terms = fields.Many2one('tender.technical.terms', string="Sourcing Technical Term")
    # description = fields.Text(string="Description", related="sourcing_technical_terms.description")
    # score_check = fields.Boolean(related='sourcing_technical_terms.score_check', string="Score ?")
    # selected_term_type = fields.Selection(selection=SEL_TYPE,string="Type", readonly=True, store=True,related="sourcing_technical_terms.sel_type")
    # selected_term_doc = fields.Boolean(string="Doc from Term?", readonly=True, store=True, related="sourcing_technical_terms.document_check")
    # data_multi_choice = fields.Many2one('tender.answer.terms', string="Value", domain="[('tt_term_id','=',sourcing_technical_terms)]")
    # data_int = fields.Integer(string="Value")
    # data_date = fields.Date(string="Value")
    # data_text = fields.Text(string="Value")
    document = fields.Binary(string="Document", attachment=True)
    score_gained = fields.Float(string="Score Gained", default=0.0)
    max_score = fields.Float(string="Maximum Score",default=0.0)
    # status = fields.Selection(selection=STATUS_TYPE, string="Status", default="await_submission", readonly=True)

    # def btn_approved(self):
    #     po_id = self.po_id
        # if self.selected_term_type == 'multi_choice':
        #     self.score = self.data_multi_choice.score
        #     self.status = 'approved'

        # if self.selected_term_type == 'text':
        #     if not self.score:
        #         raise UserError('Please manually add score before approving')
        #     self.status = 'approved'

        # if self.selected_term_type == 'int':
        #     ans_obj = self.sourcing_technical_terms.answers.sorted(lambda a: a.from_int)
        #     lo,hi = [None, None]
        #     for ans in ans_obj:
        #         if not lo and not hi:
        #             lo,hi = [ans, ans]
        #             continue
        #         if ans.from_int < lo.from_int: lo = ans
        #         if ans.to_int > hi.to_int: hi = ans
        #     for i, ans in enumerate(ans_obj):
        #         check = self.data_int
        #         frm = ans.from_int
        #         to = ans.to_int
        #         if check <= lo.from_int:
        #             self.score = lo.score
        #             break
        #         if check >= hi.to_int:
        #             self.score = hi.score
        #             break
        #         if check >= frm and check <= to:
        #             self.score = ans.score
        #             break
        #         if check > ans_obj[i-1].to_int and check < ans_obj[i].from_int:
        #             self.score = ans_obj[i].score
        #             break
        #     self.status = 'approved'

        # if self.selected_term_type == 'date':
        #     ans_obj = self.sourcing_technical_terms.answers.sorted(lambda a: a.from_date)
        #     lo,hi = [None, None]
        #     for ans in ans_obj:
        #         if not lo and not hi:
        #             lo,hi = [ans, ans]
        #             continue
        #         if ans.from_date < lo.from_date: lo = ans
        #         if ans.to_date > hi.to_date: hi = ans

        #     for i, ans in enumerate(ans_obj):
        #         check = self.data_date
        #         frm = ans.from_date
        #         to = ans.to_date
        #         if check <= lo.from_date:
        #             self.score = lo.score
        #             break
        #         if check >= hi.to_date:
        #             self.score = hi.score
        #             break
        #         if check >= frm and check <= to:
        #             self.score = ans.score
        #             break
        #         if check > ans_obj[i-1].to_date and check < ans_obj[i].from_date:
        #             self.score = ans_obj[i].score
        #             break
        #     self.status = 'approved'
        # _logger.warning("\n\n::::::: APPROVED ARR::: %s" % po_id.technical_terms)
        # po_id.total_score += self.score
        # # change PO state -> approved if all sourcings approved
        # if all(t.status in ['approved','rejected'] for t in self.po_id.technical_terms):
        #     _logger.warning("ALL called")
        #     po_id.state = 'approved'
        #     _logger.warning("\n\n:::: TOTAL SCORE:: %s" % po_id.total_score)
        #     _logger.warning("\n\n:::: QUAL SCORE:: %s" % po_id.requisition_id.type_id.qualifying_score)
        #     if po_id.total_score >= po_id.requisition_id.type_id.qualifying_score:
        #         po_id.is_qualified = True
        #     else:
        #         po_id.is_qualified = False
        #     return {
        #             'type': 'ir.actions.client',
        #             'tag': 'reload',
        #             }

    # def btn_rejected(self):
    #     self.status = 'rejected'
    #     self.score_gained = 0.0
    #     if all(t.status in ['approved','rejected'] for t in self.po_id.technical_terms):
    #         _logger.warning("ALL called")
    #         self.po_id.state = 'approved'
    #         return {
    #                 'type': 'ir.actions.client',
    #                 'tag': 'reload',
    #                 }

    # def btn_resubmit(self):
    #     self.po_id.state = 'sent'
    #     self.po_id.reset_terms()
    #     return {
    #             'type': 'ir.actions.client',
    #             'tag': 'reload',
    #             }
