# -*- coding: utf-8 -*-
from odoo.exceptions import Warning, UserError
from odoo import models, fields, api
import logging
_logger = logging.getLogger(__name__)

ACCEPT_SEL = [('accepted', 'Approved'),('rejected', 'Rejected'), ('new', 'New')]
REG_STATUS_SEL = [('old', 'Already-Registered'),('new','New Vendor')]
class PurchaseRequisitionBids(models.Model):
    _name="purchase.requisition.bids"

    requisition_id = fields.Many2one('purchase.requisition', string="Requisition ID")
    supplier_name = fields.Many2one('res.partner', string="Supplier Name")
    address = fields.Text(string="Address/ Location")
    supplier_profile = fields.Char(string="Supplier Profile")
    achivements = fields.Char(string="Achievements/ Related Experience")
    remarks = fields.Text(string="Remarks")
    single_source_doc = fields.Binary(attachment=True, string="Single Source Document")

    accept_status = fields.Selection(selection=ACCEPT_SEL, string="Status", default='new', readonly=True)
    reg_status = fields.Selection(selection=REG_STATUS_SEL, string="Registration Status", default='old')
    # if vendor is unregistered
    new_sup_name = fields.Char(string="New Supplier Name")
    new_sup_email = fields.Char(string="New Supplier Email")
    new_sup_passwd = fields.Char(string="Password")

    # purhcase_man = fields.Char(string="Purchase Manager")
    # pm_date = fields.Date(string="Date")
    # purchase_opr_director = fields.Char(string="Purchase Operation Director")
    # pod_date = fields.Date(string="Data")

    def btn_tree_accept(self):
        self.accept_status = 'accepted'
        pr_requisition_obj = self.env['purchase.requisition'].search(
                                [('id', '=', self.requisition_id.id)])
        temp = 0
        for pr_line in self.requisition_id.pr1_bid_ids:
            if pr_line.accept_status == 'accepted':
                temp += 1
            elif pr_line.accept_status == 'rejected':
                temp += 1
        if temp == len(self.requisition_id.pr1_bid_ids):
            pr_requisition_obj.write({'state': 'in_progress'})

    def btn_tree_reject(self):
        self.accept_status = 'rejected'
        pr_requisition_obj = self.env['purchase.requisition'].search(
                                [('id', '=', self.requisition_id.id)])
        temp = 0
        for pr_line in self.requisition_id.pr1_bid_ids:
            if pr_line.accept_status == 'rejected':
                temp += 1
            elif pr_line.accept_status == 'accepted':
                temp += 1
        if temp == len(self.requisition_id.pr1_bid_ids):
            pr_requisition_obj.write({'state': 'in_progress'})

    @api.onchange('supplier_name')
    def _onchange_supplier_name(self):
        sup = self.supplier_name
        if not sup:
            return
        addr = [sup.street, sup.street2, sup.city, sup.state_id.name, sup.country_id.name, sup.zip]
        # _logger.warning('\n\n:::: ADDR:: %s' % addr)
        self.address = ('\n').join(x for x in addr if x)

    @api.onchange('reg_status')
    def _onchange_reg_status(self):
        self.supplier_name = ''
        self.address = ''
        self.supplier_profile = ''
        self.achivements = ''
        self.remarks = ''
        self.single_source_doc = ''
        self.new_sup_name = ''
        self.new_sup_email = ''
        self.new_sup_passwd = ''