# -*- coding: utf-8 -*-

from . import res_partner
from . import purchase_order
from . import purchase_requisition
from . import vendor_assessment_ranking
from . import purchase_requisition_bids