# -*- coding: utf-8 -*-
from odoo.exceptions import Warning, UserError
from odoo import models, fields, api
import logging
_logger = logging.getLogger(__name__)

state_options = [
    ('draft', 'RFQ'),
    ('sent', 'RFQ Sent'),
    ('to approve', 'To Approve'),
    ('awaiting_approval', 'Awaiting Approval'),
    ('approved', 'Approved'),
    ('purchase', 'Purchase Order'),
    ('done', 'Locked'),
    ('cancel', 'Cancelled')
    ]

class VendorAssessmentRanking(models.Model):
    _name = 'vendor.tender.ranks'

    po_id = fields.Many2one('purchase.order', string="Order Reference")
    requisition_id = fields.Many2one('purchase.requisition', string="Requisition ID")
    name = fields.Char(related="po_id.name")
    state = fields.Selection(selection=state_options, string="Status", related="po_id.state")
    partner_id = fields.Many2one(related="po_id.partner_id")
    m_curreny_id = fields.Many2one(string="Currency ID", related="po_id.currency_id")
    amount_total = fields.Monetary(string="1st Quote", related="po_id.amount_total", currency_field='m_curreny_id')
    is_qualified =  fields.Boolean(string="Qualified?", related="po_id.is_qualified")
    total_score = fields.Float(string="Total Score")
    technical_rating = fields.Float(string="Technical Rating")
    commercial_rating = fields.Float(string="Commercial Rating")
    final_score = fields.Float(string="Final Score")
    presentation_points = fields.Float(string="Presentation Points", related="po_id.presentation_points")
    pseduo_rank = fields.Integer(string="P-Rank")
    rank = fields.Integer(string="Rank")
    quote_2 = fields.Monetary(string="2nd Quote", currency_field='m_curreny_id')
    quote_follow = fields.Monetary(string="Follow on Quote", currency_field='m_curreny_id')

