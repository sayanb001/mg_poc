# -*- coding: utf-8 -*-
from odoo.exceptions import Warning, UserError
from odoo import models, fields, api, _
import logging
_logger = logging.getLogger(__name__)
from odoo.tools.misc import formatLang, get_lang


PURCHASE_REQUISITION_STATES = [
    ('draft', 'Draft'),
    ('bidder_approval','Bidder Approval'),
    ('ongoing', 'Ongoing'),
    ('in_progress', 'Confirmed'),
    ('technical_review','Technical Review'),
    ('target_rate_approval','Target Rate Approval'),
    ('on_hold','On Hold'),
    ('open', 'Bid Selection'),
    ('done', 'Closed'),
    ('cancel', 'Cancelled')
]

class PurchaseRequisitionType(models.Model):
    _inherit = ['purchase.requisition.type']

    @api.constrains('default_technical_bid')
    def _check_input_range(self):
        if self.default_technical_bid < 0 or self.default_technical_bid > 100:
            raise UserError("Default Technical Bid must be between 0 and 100")

    techno_com_bid = fields.Boolean(string="Techno-commercial Bid")
    config_terms = fields.Many2many('technical.term.config', 'technical_term_config_rel', string="Pre-Configured Terms")
    default_technical_bid = fields.Float(string="Default Technical Bid %")
    qualifying_score = fields.Float(string="Qualifying Score")
    total_score = fields.Float(string="Total Score")

class PurchaseRequisition(models.Model):
    _inherit = ['purchase.requisition']

    def calc_com_bid(self):
        self.commercial_bid = 100.00 - self.technical_bid

    @api.onchange('type_id')
    def _onchange_type_id(self):
        self.technical_bid = self.type_id.default_technical_bid

    # @api.onchange('technical_terms')
    # def _compute_complete_score(self):
    #     _logger.warning("\n\n::::technical_terms changed")
    #     self.complete_score = sum(term.max_score for term in self.technical_terms)

    techno_com_bid = fields.Boolean(string="Techno-commercial Bid", related="type_id.techno_com_bid")
    technical_bid = fields.Float(string="Technical Bid %",)
    commercial_bid = fields.Float(string="Commercial Bid%", readonly=True, compute="calc_com_bid")
    # complete_score = fields.Float(string="Complete Score", compute="_compute_complete_score")
    sourcing_date = fields.Date (string="Sourcing Commencement Date")
    # technical_terms = fields.One2many('tender.technical.terms', 'requisition_id', string="Technical Terms", copy=True)
    # related_rfq_ids =  fields.One2many('purchase.order', 'requisition_id', string="Related RFQs")
    related_rfq_ids =  fields.One2many('vendor.tender.ranks', 'requisition_id', string="Related RFQs")
    pr1_bid_ids = fields.One2many('purchase.requisition.bids', 'requisition_id', string="Bids")
    state = fields.Selection(selection=PURCHASE_REQUISITION_STATES, string="Status", tracking=True, required=True,
                              copy=False, default='draft')
    state_blanket_order = fields.Selection(PURCHASE_REQUISITION_STATES, compute='_set_state')


    #bidding_list
    target_value = fields.Float(string="Target Value")
    pr_budget = fields.Float(string="PR Budget")
    pr_tax = fields.Selection(selection=[('w_tax', 'With Tax'), ('wo_tax', 'Without Tax')], string="Tax")
    meeting_date = fields.Date(string="Meeting Date")
    currency_id = fields.Many2one('res.currency', string="Currency")
    exch_rate = fields.Float(string="Exchange Rate", default = 1.0)
    buyer = fields.Char(string="Buyer") # res.partner

    


    # def btn_gen_bids(self):
    #     pass

    @api.depends('state')
    def _set_state(self):
        for requisition in self:
            requisition.state_blanket_order = requisition.state

    def send_technical_review(self):
        self.write({'state': 'technical_review'})

    def approve(self):
        self.write({'state': 'open'})

    def technical_review_completed(self):
        self.write({'state': 'target_rate_approval'})

    def reject_approval(self):
        self.write({'state': 'on_hold'})

    def close_sourcing(self):
        self.write({'state': 'done'})

    def approve_target_rate(self):
        self.write({'state': 'open'})

    def btn_invite_bidders(self):
        if self.pr1_bid_ids:
            for bid_data in self.pr1_bid_ids:
                if bid_data.accept_status == 'accepted':
                    print('\n\n::::::::::::::::118:::',bid_data.supplier_name)
                    user_id = self.env['res.users'].sudo().search([('partner_id', '=', bid_data.supplier_name.id)])
                    print('\n\n::::::::::::::::118:::',user_id)
                    group_portal = self.env.ref('vendor_portal.group_purchase_vendor_user')
                    if not bid_data.supplier_name: # new  users
                        user_data = {}
                        new_sup_name = bid_data.new_sup_name
                        new_sup_email = bid_data.new_sup_email
                        new_sup_passwd = bid_data.new_sup_passwd
                        user_data = {
                            'name': new_sup_name,
                            'login': new_sup_email,
                            'password': new_sup_passwd,
                        }
                        user_email_exist = self.env['res.users'].search([('login', '=', user_data['login'])])                        
                        print('\n\n::::::::131:::::::::::',user_email_exist)
                        if not user_email_exist:
                            user_id = self.env['res.users'].create(user_data)
                            user_id.write({'groups_id':[(6, 0, [group_portal.id])]})
                            print('\n\n::::::::::134:::::::::',user_id)

                        # else:
                        #     user_id = self.env['res.users'].search('partner_id', '=', bid_data.supplier_name.id)
                    print('\n\n::::::::::138:::::::::',user_id)
                    if user_id.partner_id:
                        purchase_data = {
                            'partner_id': user_id.partner_id.id,
                            'state': 'sent',
                            'requisition_id':self.id,
                            'origin':self.name,
                            'techno_com_bid': True,
                        }
                        purchase_id = self.env['purchase.order'].create(purchase_data)
                        for pa_line in self.line_ids:
                            product_lang = pa_line.product_id.with_context(
                                lang=get_lang(self.env, user_id.partner_id.lang).code,
                                partner_id=user_id.partner_id.id,
                                company_id=self.company_id.id,
                            )
                            name = product_lang.display_name
                            if product_lang.description_purchase:
                                name += '\n' + product_lang.description_purchase
                            product_uom = pa_line.product_id.uom_po_id or pa_line.product_id.uom_id
                            po_line_data = {
                                'order_id':purchase_id.id,
                                'product_id': pa_line.product_id.id,
                                'product_uom':product_uom.id,
                                'name':name,
                                'product_qty':pa_line.product_qty,
                                'price_unit':pa_line.price_unit,
                                'date_planned':pa_line.schedule_date,
                            }
                            purchase_line_id = self.env['purchase.order.line'].create(po_line_data)
                            vendor_regis_id = self.env['vendor.registration'].search(
                                [('res_user_id', '=', user_id.id)])
                        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
                        po_url =base_url + '/web#id=%d&view_type=form&model=%s' % (purchase_id.id, purchase_id._name)
                        registration_url = base_url + '/web#id=%d&view_type=form&model=%s' % (vendor_regis_id.id, vendor_regis_id._name)
                        template_id = self.env.ref('mg_purchase.email_template_invite_supplier')
                        old_template_id = self.env.ref('mg_purchase.email_template_invite_old_supplier')
                        if bid_data.reg_status == 'new':
                            template_id.with_context(email_to=user_data['login'],
                                supplier_name=new_sup_name,new_sup_passwd=new_sup_passwd,po_url=po_url,
                                registration_url=registration_url).send_mail(self.id)
                        else:
                            old_template_id.with_context(email_to=user_id.partner_id.email,
                                supplier_name=bid_data.supplier_name.name,po_url=po_url,
                                registration_url=registration_url).send_mail(self.id)


    # def show_terms_btn(self):
    #     ''' For Existing Terms Button'''
    #     return {
    #         'name': 'Technical Term Config',
    #         'type': 'ir.actions.act_window',
    #         'view_mode': 'form',
    #         'res_model': 'technical.term.config.wizard',
    #         'view_id': self.env.ref('mg_purchase.technical_term_config_wizard3').id,
    #         'target': 'new',
    #     }

    # def check_complete_score(self, vals):
    #     _logger.warning("total_sum :: %s " % self.type_id.total_score)
    #     score_sum = 0
    #     if not 'technical_terms' in vals[0]:
    #         return True
    #     for v in vals[0]['technical_terms']:
    #         if 'process_write' not in self._context:
    #             score_sum += v[2]['max_score']
    #         else:
    #             if isinstance(v[2], dict) and 'max_score' in v[2]:
    #                 score_sum += v[2]['max_score']
    #     return score_sum == self.type_id.total_score

    def set_ranks(self, rfqs):
        for r in rfqs:
            if r.pseduo_rank < 10000:
                r.rank = r.pseduo_rank

    # def rank_by_final_score(self, dict_final, rfqs, final_score_collision): #future: refactoring btn_access_vendors (old)
    #     '''
    #     Returns True if ranking process is successfull else returns False
    #     '''
    #     self.init_dict(dict_final, 'final_score')
    #     for r in rfqs:
    #         try:
    #             dict_final[r.final_score].append(r)
    #         except:
    #             continue
    #     _logger.warning('\n\n:::: dict_final:: %s' % dict_final)
    #     for d in dict_final:
    #         if len(dict_final[d]) > 1:
    #             final_score_collision.extend(dict_final[d])
    #         else:
    #             ok_list.extend(dict_final[d])
    #     _logger.warning("\n:::: ok_list %s" % ok_list)
    #     if final_score_collision:
    #         return False
    #     ok_list = sorted(ok_list, key=lambda x: x.final_score)[::-1]
    #     for i,x in enumerate(ok_list):
    #         x.pseduo_rank = i+1
    #     return True

    # def btn_assess_vendors(self): #OLD author: Sayan
        # rfqs = self.related_rfq_ids
    #     if not rfqs:
    #         raise UserError('No Vendor RFQs yet to assess.')
    #     total = rfqs.filtered(lambda rfq: rfq.is_qualified)
    #     filter_passable =  rfqs.filtered(lambda rfq: rfq.state in ['approved', 'cancel', 'purchase'] and rfq.is_qualified)
    #     if len(total)!= len(filter_passable):
    #         raise UserError("There are few RFQ's still left with unapproved status. Please resolve them.")
    #     Th = filter_passable.sorted(lambda r: r.total_score).mapped('total_score')[::-1][0]
    #     Cl = filter_passable.sorted(lambda r: r.amount_total).mapped('amount_total')[0]
    #     _logger.warning('\nLowest Financial:: %s' % Cl)
    #     _logger.warning('\nHighest Technical:: %s' % Th)
    #     for rfq in filter_passable:
    #         rfq.commercial_rating = Cl / rfq.amount_total * 100
    #         rfq.technical_rating = rfq.total_score / Th * 100
    #         rfq.final_score = (0.01 * self.technical_bid * rfq.technical_rating) + (0.01 * self.commercial_bid * rfq.commercial_rating)

    #     pseudo_unrank = 10**4 -1
    #     #TODO final -> technical -> presentation
    #     for r in rfqs:
    #         if r not in filter_passable:
    #             pseudo_unrank +=1
    #             r.update({'pseduo_rank': pseudo_unrank})
    #     final_score_collision = []
    #     sort_final_score = filter_passable.sorted(key=lambda x: x.final_score, reverse=True) #.mapped('name')
    #     i = 1
    #     sort_final_score[0].pseduo_rank = i; i += 1
    #     for j in range(1,len(sort_final_score)):
    #         if sort_final_score[j].final_score == sort_final_score[j-1].final_score:
    #             final_score_collision.append(sort_final_score[j])
    #             final_score_collision.append(sort_final_score[j-1])
    #             sort_final_score[j].pseduo_rank = sort_final_score[j-1].pseduo_rank
    #             i += 1
    #         else:
    #             sort_final_score[j].pseduo_rank = i
    #             i += 1
    #     if not final_score_collision:
    #         self.set_ranks(rfqs)
    #         return

    #     final_score_collision = list(set(final_score_collision))
    #     technical_score_collision = []
    #     sort_tech_score = sorted(final_score_collision, key=lambda x: x.technical_rating, reverse=True)
    #     i = 1
    #     sort_tech_score[0].pseduo_rank = i; i += 1
    #     for j in range(1, len(sort_tech_score)):
    #         if sort_tech_score[j].technical_rating == sort_tech_score[j-1].technical_rating:
    #             technical_score_collision.append(sort_tech_score[j])
    #             technical_score_collision.append(sort_tech_score[j-1])
    #             sort_tech_score[j].pseduo_rank = sort_tech_score[j-1].pseduo_rank
    #             i += 1
    #         else:
    #             sort_tech_score[j].pseduo_rank = i
    #             i += 1
    #     if not technical_score_collision:
    #         self.set_ranks(rfqs)
    #         return

    #     sort_tech_score = list(set(sort_tech_score))
    #     presen_point_collision = []
    #     sort_presen_score = sorted(sort_tech_score, key=lambda x: x.presentation_points, reverse=True)
    #     i = 1
    #     sort_presen_score[0].pseduo_rank = i; i += 1
    #     for j in range(1, len(sort_presen_score)):
    #         if sort_presen_score[j].presentation_points == sort_presen_score[j-1].presentation_points:
    #             presen_point_collision.append(sort_tech_score[j])
    #             presen_point_collision.append(sort_tech_score[j-1])
    #             sort_presen_score[j].pseduo_rank = sort_presen_score[j-1].pseduo_rank
    #             i += 1
    #         else:
    #             sort_presen_score[j].pseduo_rank = i
    #             i += 1
    #     self.set_ranks(rfqs)
    #     return

    # def btn_assess_vendors(self):
    #     rfqs = self.related_rfq_ids
    #     if not rfqs:
    #         raise UserError('No Vendor RFQs yet to assess.')
    #     total = rfqs.filtered(lambda rfq: rfq.is_qualified)
    #     filter_passable =  rfqs.filtered(lambda rfq: rfq.state in ['approved', 'cancel', 'purchase'] and rfq.is_qualified)
    #     if len(total)!= len(filter_passable):
    #         raise UserError("There are few RFQ's still left with unapproved status. Please resolve them.")
    #     Th = filter_passable.sorted(lambda r: r.total_score).mapped('total_score')[::-1][0]
    #     Cl = filter_passable.sorted(lambda r: r.amount_total).mapped('amount_total')[0]
    #     _logger.warning('\nLowest Financial:: %s' % Cl)
    #     _logger.warning('\nHighest Technical:: %s' % Th)
    #     for rfq in filter_passable:
    #         rfq.commercial_rating = Cl / rfq.amount_total * 100
    #         rfq.technical_rating = rfq.total_score / Th * 100
    #         rfq.final_score = (0.01 * self.technical_bid * rfq.technical_rating) + (0.01 * self.commercial_bid * rfq.commercial_rating)

    #     pseudo_unrank = 10**4 -1
    #     #TODO final -> technical -> presentation
    #     for r in rfqs:
    #         if r not in filter_passable:
    #             pseudo_unrank +=1
    #             r.update({'pseduo_rank': pseudo_unrank})
        
    #     data = []
    #     # item = {
    #     #     'id': 11,
    #     #     'tech': 100,
    #     #     'final' : 50,
    #     #     'pres' : 10,
    #     # }
    #     for rfq in filter_passable:
    #         data.append({'id': rfq.id, 'tech': rfq.technical_rating, 'final': rfq.final_score, 'pres': rfq.presentation_points})
    #     data_len = len(data)
    #     tech_dic = {}
    #     final_dic = {}
    #     pres_dic = {}
    #     ranking_data_dic = {}
    #     for i in range(0, data_len):
    #         final_dic[data[i]['id']] = data[i]['final']
    #         tech_dic[data[i]['id']] = data[i]['tech']
    #         pres_dic[data[i]['id']] = data[i]['pres']
    #     #tech_dic = sorted(tech_dic.items(), key = lambda kv:(kv[1], kv[0]), reverse=True)
    #     final_dic = sorted(final_dic.items(), key = lambda kv:(kv[1], kv[0]), reverse=True)
    #     #pres_dic = sorted(pres_dic.items(), key = lambda kv:(kv[1], kv[0]), reverse=True)
    #     print	('final_dic::::',final_dic)
    #     print	('tech_dic::::',tech_dic)
    #     print	('pres_dic::::',pres_dic)

    #     next_rank = main_rank = 0
    #     score_list = []
    #     for data in final_dic:
    #         next_rank = next_rank + 1
    #         if data[1] in score_list:
    #             ranking_data_dic[data[0]] = main_rank
    #             score_list.append(data[1])
    #         else:
    #             main_rank = next_rank
    #             ranking_data_dic[data[0]] = main_rank
    #             score_list.append(data[1])

    #     print ("ranking_data_dic::::::::::",ranking_data_dic)

    #     rev_dict = {} 
    #     for key, value in ranking_data_dic.items(): 
    #         if value in rev_dict:
    #             rev_dict[value].append(key)
    #         else:
    #             rev_dict[value] = [key]
    #     print('rev_dict:::::;',rev_dict)
    #     for key, value in rev_dict.items(): 
    #         if len(value) > 1:
    #             temp_dic = {}
    #             for i in range(0, len(value)):
    #                 rec_id = value[i]
    #                 temp_dic[rec_id] = tech_dic[rec_id]
    #             print ('temp_dic::::::',temp_dic)
    #             temp_dic = sorted(temp_dic.items(), key = lambda kv:(kv[1], kv[0]), reverse=True)
    #             print ('temp_dic::::::',temp_dic)
    #             next_rank = main_rank = key
    #             score_list = []
    #             for data in temp_dic:
    #                 if data[1] in score_list:
    #                     ranking_data_dic[data[0]] = main_rank
    #                     score_list.append(data[1])
    #                 else:
    #                     main_rank = next_rank
    #                     ranking_data_dic[data[0]] = main_rank
    #                     score_list.append(data[1])
    #                 next_rank = next_rank + 1
                    
    #     print ("ranking_data_dic after tech::::::::::",ranking_data_dic)

    #     rev_dict = {} 
    #     for key, value in ranking_data_dic.items(): 
    #         if value in rev_dict:
    #             rev_dict[value].append(key)
    #         else:
    #             rev_dict[value] = [key]
    #     print('rev_dict:::::;',rev_dict)
    #     for key, value in rev_dict.items(): 
    #         if len(value) > 1:
    #             temp_dic = {}
    #             for i in range(0, len(value)):
    #                 rec_id = value[i]
    #                 temp_dic[rec_id] = pres_dic[rec_id]
    #             print ('temp_dic::::::',temp_dic)
    #             temp_dic = sorted(temp_dic.items(), key = lambda kv:(kv[1], kv[0]), reverse=True)
    #             print ('temp_dic::::::',temp_dic)
    #             next_rank = main_rank = key
    #             score_list = []
    #             for data in temp_dic:
    #                 if data[1] in score_list:
    #                     ranking_data_dic[data[0]] = main_rank
    #                     score_list.append(data[1])
    #                 else:
    #                     main_rank = next_rank
    #                     ranking_data_dic[data[0]] = main_rank
    #                     score_list.append(data[1])
    #                 next_rank = next_rank + 1

    #     rfq_obj = self.env['vendor.tender.ranks']
    #     print ("ranking_data_dic after pres::::::::::",ranking_data_dic)
    #     for key,val in ranking_data_dic.items():
    #         rfq_obj.browse(key).write({'rank': val})

    # def init_dict(self, dict_obj, field_name):
    #     items = self.related_rfq_ids.mapped(field_name)
    #     for item in items:
    #         if item:
    #             dict_obj.update({item:[]})

    def action_in_progress(self):
        self.ensure_one()
        total_score = self.type_id.total_score
        _logger.warning("total_score2::: %s" %total_score)
        # if(self.complete_score != total_score):
        #     raise UserError(f"Complete Score: Sum of Max Score in Technical Terms tab.\nComplete score should be exactly {total_score}")
        if not all(obj.line_ids for obj in self):
            raise UserError(_("You cannot confirm agreement '%s' because there is no product line.") % self.name)
        if self.type_id.quantity_copy == 'none' and self.vendor_id:
            for requisition_line in self.line_ids:
                if requisition_line.price_unit <= 0.0:
                    raise UserError(_('You cannot confirm the blanket order without price.'))
                if requisition_line.product_qty <= 0.0:
                    raise UserError(_('You cannot confirm the blanket order without quantity.'))
                requisition_line.create_supplier_info()
            self.write({'state': 'ongoing'})
        else:
            self.write({'state': 'bidder_approval'})
        # Set the sequence number regarding the requisition type
        if self.name == 'New':
            if self.is_quantity_copy != 'none':
                self.name = self.env['ir.sequence'].next_by_code('purchase.requisition.purchase.tender')
            else:
                self.name = self.env['ir.sequence'].next_by_code('purchase.requisition.blanket.order')

    # @api.model_create_multi
    # def create(self, vals):
    #     req_type_score = self.type_id.search([('id','=', vals[0]['type_id'])]).total_score
    #     _logger.warning("\n\n::::::::::::::CREATE VALS %s" % vals)
    #     # cs_check = self.check_complete_score(vals)
    #     # if not cs_check:
    #     #     raise UserError(f"Complete Score: Sum of Max Score in Technical Terms tab.\nComplete score should be exactly {req_type_score}")
    #     return super(PurchaseRequisition, self).create(vals)

    # def write(self,vals):
    #     #TODO check unchanged values not updating correctly, unsure
    #     _logger.warning("\n\n::::WRITE VALS::: %s" % vals)
    #     all_lines = self.env['tender.technical.terms'].search([('requisition_id', '=', self.id)])
    #     lines_score_dict = {t.id:t.max_score for t in all_lines} # for O(1) search times
    #     prepare = []; to_pop = []
    #     if 'technical_terms' in vals:
    #         for i, val in enumerate(vals['technical_terms']):
    #             if val[0] != 2 and not val[2]: # [2, id1, {}] - disallowed, skip deleted lines from o2m
    #                 old_id = val[1]
    #                 to_pop.append(i)
    #                 prepare.append([1,old_id,{'max_score': lines_score_dict[old_id]}])
    #         for i in to_pop[::-1]: #delete from reverse, so that index doesn't get messed up
    #             del vals['technical_terms'][i]
    #         vals['technical_terms'].extend(prepare)
    #         cs_check = self.with_context({'process_write':1}).check_complete_score([vals])
    #         if not cs_check:
    #             raise UserError("Complete Score: Sum of Max Score in Technical Terms tab.\nComplete score should be exactly 100")
    #     return super(PurchaseRequisition, self).write(vals)
