# -*- coding: utf-8 -*-
{
    'name': "RFQ Extension",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['purchase_requisition', 'technical_term_config','mail'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/purchase_requisition.xml',
        'views/purchase_requisition_type.xml',
        'views/purchase_order.xml',
        'views/danger_line_bg.xml',
        'views/invite_supplier_mail.xml',
        'views/res_partner_form_view.xml'
    ],
}